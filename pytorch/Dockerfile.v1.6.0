FROM arm32v7/ubuntu:bionic
RUN apt update

RUN export DEBIAN_FRONTEND=noninteractive
RUN ln -fs /usr/share/zoneinfo/Europe/Prague /etc/localtime
RUN apt-get install -y tzdata
RUN dpkg-reconfigure --frontend noninteractive tzdata
RUN apt -y install python3-sklearn git libopenblas-dev libblas-dev m4 cmake cython python3-dev python3-pip python3-yaml python3-numpy python3-pillow python3-setuptools cmake build-essential pkg-config libgoogle-perftools-dev

ENV USE_CUDA=0
ENV USE_DISTRIBUTED=0
ENV USE_MKLDNN=0
ENV USE_NNPACK=0
ENV USE_QNNPACK=0
ENV USE_NUMPY=1
ENV NO_CUDA=1
ENV NO_DISTRIBUTED=1
ENV NO_MKLDNN=1
ENV NO_NNPACK=1
ENV NO_QNNPACK=1

RUN mkdir /pytorch_install && \
    cd /pytorch_install && \
    git clone --recursive https://github.com/pytorch/pytorch --branch=v1.6.0 && \
    cd pytorch && \
    python3 setup.py install && \
    rm -rf /pytorch_install
RUN mkdir /pytorch_install && \
    cd /pytorch_install && \
    git clone --recursive https://github.com/pytorch/vision.git --branch=v0.7.0 && \
    cd vision && \
    python3 setup.py install && \
    rm -rf /pytorch_install
RUN mkdir /pytorch_install && \
    cd /pytorch_install && \
    git clone https://github.com/google/sentencepiece.git && \
    cd sentencepiece/ && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j $(nproc) && \
    make install && \
    cd ../python && \
    python3 setup.py build && \
    python3 setup.py install && \
    rm -rf /pytorch_install
RUN mkdir /pytorch_install && \
    cd /pytorch_install && \
    git clone --recursive https://github.com/pytorch/text.git --branch=0.5.0 && \
    cd text && \
    python3 setup.py install && \
    rm -rf /pytorch_install
